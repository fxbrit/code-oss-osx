#!/bin/sh

repo="https://github.com/microsoft/vscode.git"

git clone $repo
cd vscode
yarn
patch product.json < ../patches/marketplace.patch
yarn gulp vscode-darwin-x64

# code-oss-osx

learn about [code-oss](https://github.com/Microsoft/vscode): basically the advantages over vscode are that it's not under MS product license, it is 100% open source and it's not built by MS itself.
the reason this repo exists is that I build it for myself so I thought why not put it out there, if you trust me more than Microsoft or you don't want to use a product under their license go ahead and enjoy.

I implement full marketplace compatibility using a patch, which is also in this repo, all the rest of the stuff is the same as if you built it from source yourself. please disable telemetry from the settings (two checkboxes, not worth the hassle to do it at build time imo).

if you want to build from source yourself with my marketplace modification:
```
git clone https://gitlab.com/fxbrit/code-oss-osx.git
./build.sh
```
it will do the full process, then drag the .app in your applications directory.

for binaries use the releases at [this github page](https://github.com/fxbrit/code-oss-releases/releases/).

have fun!
